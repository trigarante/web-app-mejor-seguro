import { Component } from '@angular/core';
import { routes } from '../../consts/routes';
import {AuthService} from '../../modulos/auth/services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  public routes: typeof routes = routes;
  public isOpenUiElements = false;
  panelOpenState = false;

  constructor(public userService: AuthService, public router: Router){
  }
  public openUiElements() {
    this.isOpenUiElements = !this.isOpenUiElements;
  }
  public signOut(): void {
    this.userService.signOut();
    this.router.navigate([this.routes.LOGIN]);
  }
}
