import {Component, EventEmitter, HostListener, Input, Output, AfterContentInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { Email, User } from '../../../../modulos/auth/models';
import { AuthService, EmailService } from '../../../../modulos/auth/services';
import { routes } from '../../../../consts';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterContentInit{
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public routers: typeof routes = routes;
  innerWidth: number;
  submenuweb = false;
  url: string;

  ngAfterContentInit() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth >= 930) {
      this.isShowSidebar.emit(false);
    }
    this.url = window.location.hash;
    if ('#' + this.routers.CONDICIONES_GENERALES === this.url
      || '#' + this.routers.ACLARACION_Y_SUGERENCIAS === this.url
      || '#' + this.routers.CANCELAR_UNA_POLIZA === this.url
      || '#' + this.routers.INSTRUCCIONES_DE_PAGO === this.url
      || '#' + this.routers.CATALOGO_DE_FACTURACION === this.url ){
      this.submenuweb = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth >= 930) {
      this.isShowSidebar.emit(false);
    }
  }

  constructor(private userService: AuthService, private router: Router) {
    this.innerWidth = window.innerWidth;
  }

  public openMenu(): void {
    if (this.innerWidth < 930) {
      this.isMenuOpened = !this.isMenuOpened;
      this.isShowSidebar.emit(this.isMenuOpened);
    }
    else{
      this.isShowSidebar.emit(false);
      this.submenuweb = !this.submenuweb;
    }
  }

  public signOut(): void {
    this.userService.signOut();

    this.router.navigate([this.routers.LOGIN]);
  }
}
