import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import {MatCardModule} from '@angular/material/card';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {PdfViewerModule} from 'ng2-pdf-viewer';

@NgModule({
  declarations: [DashboardComponent, DashboardPageComponent],
    imports: [
        CommonModule,
        MatCardModule,
        SharedModule,
        MatToolbarModule,
        PdfViewerModule
    ],
  providers: [
  ]
})
export class DashboardModule { }
