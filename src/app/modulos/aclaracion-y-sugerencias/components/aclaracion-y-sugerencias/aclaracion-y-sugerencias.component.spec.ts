import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AclaracionYSugerenciasComponent } from './aclaracion-y-sugerencias.component';

describe('AclaracionYSugerenciasComponent', () => {
  let component: AclaracionYSugerenciasComponent;
  let fixture: ComponentFixture<AclaracionYSugerenciasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AclaracionYSugerenciasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AclaracionYSugerenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
