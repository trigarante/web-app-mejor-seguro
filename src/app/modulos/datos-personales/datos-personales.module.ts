import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';

import { DatosPersonalesPageComponent } from './containers';
import { DatosPersonalesRoutingModule } from './datos-personales-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DatosPersonalesComponent } from './components/datos-personales/datos-personales.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';

@NgModule({
  declarations: [DatosPersonalesPageComponent, DatosPersonalesComponent],
  imports: [
    CommonModule,
    DatosPersonalesRoutingModule,
    MatCardModule,
    MatToolbarModule,
    SharedModule,
    MatProgressBarModule
  ]
})
export class DatosPersonalesModule { }
