import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelarUnaPolizaPageComponent } from './cancelar-una-poliza-page.component';

describe('CancelarUnaPolizaPageComponent', () => {
  let component: CancelarUnaPolizaPageComponent;
  let fixture: ComponentFixture<CancelarUnaPolizaPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelarUnaPolizaPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelarUnaPolizaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
