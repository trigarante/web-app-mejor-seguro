import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CancelarUnaPolizaPageComponent} from './containers';

const routes: Routes = [
  {path: '',
    component: CancelarUnaPolizaPageComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancelarUnaPolizaRoutingModule { }
