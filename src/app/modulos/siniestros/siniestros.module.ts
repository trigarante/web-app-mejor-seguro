import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiniestrosRoutingModule } from './siniestros-routing.module';
import { SiniestrosComponent } from './components/siniestros/siniestros.component';
import { SiniestrosPageComponent } from './containers/siniestros-page/siniestros-page.component';
import {SharedModule} from "../../shared/shared.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [SiniestrosComponent, SiniestrosPageComponent],
    imports: [
        CommonModule,
        SiniestrosRoutingModule,
        SharedModule,
        MatToolbarModule,
        MatCardModule
    ]
})
export class SiniestrosModule { }
