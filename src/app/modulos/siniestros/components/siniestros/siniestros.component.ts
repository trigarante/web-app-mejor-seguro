import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-siniestros',
  templateUrl: './siniestros.component.html',
  styleUrls: ['./siniestros.component.scss']
})
export class SiniestrosComponent implements OnInit {
  aseguradoras: Array<{ id: string, tel: string, telOption: string, url: string }>;
  constructor() {
    this.aseguradoras = [
      {id: 'ABA', tel: '800 712 2828', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/aba.svg'},
      {id: 'SEGUROS AFIRME', tel: '800 723 4763', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/afirme.svg'},
      {id: 'ANA SEGUROS', tel: '800 911 2627 ', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/ana.svg'},
      {id: 'AXA', tel: '800 900 1292', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/axa.svg'},
      {id: 'BANORTE', tel: '800 500 1500', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/banorte.svg'},
      {id: 'GENERAL DE SEGUROS', tel: '800 472 7696', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/general.svg'},
      {id: 'GNP', tel: '800 400 9000', telOption: 'Marcar el siguiente número\nOpción 1-5', url: 'assets/img/icons/gnp.svg'},
      {id: 'HDI', tel: '800 019 6000', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/hdi.svg'},
      {id: 'INBURSA', tel: '800 909 0000', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/inbursa.svg'},
      {id: 'MAPFRE', tel: '55 5950 7777 ', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/mapfre.svg'},
      {id: 'MIGO', tel: '800 00 90 000', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/migo.svg'},
      {id: 'EL POTOSI', tel: '800 00 90 000', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/elpotosi.svg'},
      {id: 'QUALITAS', tel: '800 800 2880', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/qualitas.svg'},
      {id: 'ZURA', tel: '800 911 7692', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/sura.svg'},
      {id: 'ZURICH', tel: '800 288 6911', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/zurich.svg'},
      {id: 'EL AGUILA', tel: '800 705 8800', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/elaguila.svg'},
      {id: 'AIG', tel: '800 001 1300', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/aig.svg'},
      {id: 'LA LATINO', tel: '800 685 1170', telOption: 'Marcar el siguiente número', url: 'assets/img/icons/lalatino.svg'}
    ];
  }

  ngOnInit(): void {
  }

}
