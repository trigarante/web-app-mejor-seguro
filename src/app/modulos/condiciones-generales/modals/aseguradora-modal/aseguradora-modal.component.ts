import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-aseguradora-modal',
  templateUrl: './aseguradora-modal.component.html',
  styleUrls: ['./aseguradora-modal.component.scss']
})
export class AseguradoraModalComponent implements OnInit {
  documentRoute: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: {route: string}) {
    this.documentRoute = data.route;
  }

  ngOnInit(): void {
  }

}
