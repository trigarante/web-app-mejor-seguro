import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AseguradoraModalComponent } from './aseguradora-modal.component';

describe('AseguradoraModalComponent', () => {
  let component: AseguradoraModalComponent;
  let fixture: ComponentFixture<AseguradoraModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AseguradoraModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AseguradoraModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
