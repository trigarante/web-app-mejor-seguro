import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CondicionesGeneralesRoutingModule } from './condiciones-generales-routing.module';
import { CondicionesGeneralesComponent } from './components/condiciones-generales/condiciones-generales.component';
import { CondicionesGeneralesPageComponent } from './containers/condiciones-generales-page/condiciones-generales-page.component';
import { AseguradoraModalComponent } from './modals/aseguradora-modal/aseguradora-modal.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {SharedModule} from "../../shared/shared.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {PdfViewerModule} from 'ng2-pdf-viewer';


@NgModule({
  declarations: [CondicionesGeneralesComponent, CondicionesGeneralesPageComponent, AseguradoraModalComponent],
  imports: [
    CommonModule,
    CondicionesGeneralesRoutingModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    SharedModule,
    MatToolbarModule,
    PdfViewerModule
  ]
})
export class CondicionesGeneralesModule { }
