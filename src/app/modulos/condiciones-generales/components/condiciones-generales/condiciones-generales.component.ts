import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AseguradoraModalComponent} from '../../modals/aseguradora-modal/aseguradora-modal.component';

@Component({
  selector: 'app-condiciones-generales',
  templateUrl: 'condiciones-generales.component.html',
  styleUrls: ['./condiciones-generales.component.scss']
})
export class CondicionesGeneralesComponent implements OnInit{
  insurersGroupCollection: Array<{ id: string, urlIMG: string, urlDocument: string }>;
  clickeado: string;
  i: number;

  constructor(public dialog: MatDialog) {
    this.insurersGroupCollection = [
      {
        id: 'ABA',
        urlIMG: 'assets/img/icons/aba.svg',
        urlDocument: 'assets/docs/condiciones-generales/aba-cg.pdf'
      },
      {
        id: 'SEGUROS AFIRME',
        urlIMG: 'assets/img/icons/afirme.svg',
        urlDocument: 'assets/docs/condiciones-generales/afirmecg.pdf'
      },
      {
        id: 'ANA SEGUROS',
        urlIMG: 'assets/img/icons/ana.svg',
        urlDocument: 'assets/docs/condiciones-generales/AnaSeguros.pdf'
      },
      {
        id: 'AXA',
        urlIMG: 'assets/img/icons/axa.svg',
        urlDocument: 'assets/docs/condiciones-generales/AXA.pdf'
      },
      {
        id: 'BANORTE',
        urlIMG: 'assets/img/icons/banorte.svg',
        urlDocument: 'assets/docs/condiciones-generales/banorte.pdf'
      },
      {
        id: 'GENERAL DE SEGUROS',
        urlIMG: 'assets/img/icons/general.svg',
        urlDocument: 'assets/docs/condiciones-generales/GeneralDeSeguros.pdf'
      },
      {
        id: 'GNP',
        urlIMG: 'assets/img/icons/gnp.svg',
        urlDocument: 'assets/docs/condiciones-generales/GNP.pdf'
      },
      {
        id: 'HDI',
        urlIMG: 'assets/img/icons/hdi.svg',
        urlDocument: 'assets/docs/condiciones-generales/HDI.pdf'
      },
      {
        id: 'INBURSA',
        urlIMG: 'assets/img/icons/inbursa.svg',
        urlDocument: ''
      },
      {
        id: 'MAPFRE',
        urlIMG: 'assets/img/icons/mapfre.svg',
        urlDocument: 'assets/docs/condiciones-generales/Mapfre.pdf'
      },
      {
        id: 'MIGO',
        urlIMG: 'assets/img/icons/migo.svg',
        urlDocument: ''
      },
      {
        id: 'EL POTOSI',
        urlIMG: 'assets/img/icons/elpotosi.svg',
        urlDocument: 'assets/docs/condiciones-generales/potosi.pdf'
      },
      {
        id: 'QUALITAS',
        urlIMG: 'assets/img/icons/qualitas.svg',
        urlDocument: 'assets/docs/condiciones-generales/Qualitas.pdf'
      },
      {
        id: 'ZURA',
        urlIMG: 'assets/img/icons/sura.svg',
        urlDocument: 'assets/docs/condiciones-generales/Sura.pdf'
      },
      {
        id: 'ZURICH',
        urlIMG: 'assets/img/icons/zurich.svg',
        urlDocument: 'assets/docs/condiciones-generales/Zurich.pdf'
      },
      {
        id: 'EL AGUILA',
        urlIMG: 'assets/img/icons/elaguila.svg',
        urlDocument: 'assets/docs/condiciones-generales/aguila.pdf'
      },
      {
        id: 'AIG',
        urlIMG: 'assets/img/icons/aig.svg',
        urlDocument: 'assets/docs/condiciones-generales/AIG.pdf'
      },
      {
        id: 'LA LATINO',
        urlIMG: 'assets/img/icons/lalatino.svg',
        urlDocument: 'assets/docs/condiciones-generales/LaLatino.pdf'
      },
      {
        id: 'Atlas Seguros',
        urlIMG: 'assets/img/icons/atlas.svg',
        urlDocument: 'assets/docs/condiciones-generales/atlasSeguros.pdf'
      },
      {
        id: 'Bx+',
        urlIMG: 'assets/img/icons/bxmas.svg',
        urlDocument: 'assets/docs/condiciones-generales/Bx+.pdf'
      }
    ];
  }

  openDialog(i) {
    this.clickeado = this.insurersGroupCollection[i].urlDocument;
    const dialogRef = this.dialog.open(AseguradoraModalComponent, {data: {route: this.clickeado}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  ngOnInit(): void {
  }
}

