import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {INSPECT_CUSTOM} from "ts-node";
import {InspeccionVehicularPageComponent} from './containers';

const routes: Routes = [
  {path: '',
    component: InspeccionVehicularPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspeccionVehicularRoutingModule { }
