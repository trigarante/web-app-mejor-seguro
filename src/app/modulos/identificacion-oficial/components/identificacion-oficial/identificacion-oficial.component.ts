import {Component, HostListener, OnInit, OnChanges} from '@angular/core';

@Component({
  selector: 'app-identificacion-oficial',
  templateUrl: './identificacion-oficial.component.html',
  styleUrls: ['./identificacion-oficial.component.scss']
})


export class IdentificacionOficialComponent implements OnInit{
  innerWidth: number;
  temp: string;
  folders = [
    {
      name: 'INE',
      imgURL: 'assets/img/INE.png',
      updated: new Date('1/1/16'),
    },
    {
      name: 'Cédula profesional',
      imgURL: 'assets/img/cedula.png',
      updated: new Date('1/28/16'),
    },
    {
      name: 'Pasaporte vigente',
      imgURL: 'assets/img/pasaporte.png',
      updated: new Date('1/17/16'),
    }
  ];
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  constructor() {
  }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth >= 930){
      this.temp = this.folders[1].imgURL;
      this.folders[1].imgURL = this.folders[2].imgURL;
      this.folders[2].imgURL = this.temp;
      this.temp = this.folders[1].name;
      this.folders[1].name = this.folders[2].name;
      this.folders[2].name = this.temp;
    }
  }
}
