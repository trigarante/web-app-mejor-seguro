import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PolizaService} from '../../../../@core/services/poliza.service';
import {routes} from '../../../../consts';

@Component({
  selector: 'app-viewer-pdf',
  templateUrl: './viewer-pdf.component.html',
  styleUrls: ['./viewer-pdf.component.scss']
})
export class ViewerPdfComponent implements OnInit, OnDestroy {
  archivoPoliza: string;
  urlPDF = 'https://login-app.mark-43.net/auth-cliente/poliza/';
  private routers: typeof routes = routes;
  constructor(private route: ActivatedRoute, private polizaService: PolizaService, private router: Router) {
    this.archivoPoliza = String(this.route.snapshot.paramMap.get('idArchivo'));
    this.urlPDF += this.archivoPoliza;
    if (localStorage.getItem('url-poliza') === null) {
      localStorage.setItem('url-poliza', this.archivoPoliza);
    } else {
      this.router.navigate([this.routers.POLIZAS]).then();
    }
  }
  ngOnInit() {
  }
  ngOnDestroy() {
    localStorage.removeItem('url-poliza');
  }
  async getPoliza() {
    try {
      const res = await this.polizaService.getDocument(localStorage.getItem('url-poliza'));
      this.savePDF(res);
    } catch (err){
    }
  }
  savePDF(res) {
    const url = window.URL.createObjectURL(res);
    window.open(url);
  }
}
