import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerPdfComponent } from './viewer-pdf.component';

describe('ViewerPdfComponent', () => {
  let component: ViewerPdfComponent;
  let fixture: ComponentFixture<ViewerPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
