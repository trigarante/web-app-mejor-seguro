import { Component, OnInit } from '@angular/core';
import {Poliza} from '../../../../@core/interfaces/poliza';
import {PolizaService} from '../../../../@core/services/poliza.service';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {routes} from '../../../../consts';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import {yearsPerPage, yearsPerRow} from '@angular/material/datepicker';

@Component({
  selector: 'app-polizas',
  templateUrl: './polizas.component.html',
  styleUrls: ['./polizas.component.scss']
})
export class PolizasComponent implements OnInit {
  private routers: typeof routes = routes;
  polizas: Poliza[] = [];
  images: Array<{ id: string; url: string; dynamicColor: string; }>;
  loaderCondition = false;
  usuarioActivo: string;
  fechaFin: any;
  constructor(private polizaService: PolizaService, private router: Router, private dialog: MatDialog) {
    this.usuarioActivo = localStorage.getItem('token');

    // ARREGLO PARTA OBTENER LAS IMAGENES POR ID EN EL SERVICIO
    this.images = [
      {id: 'ABA', url: 'assets/img/icons/aba.svg', dynamicColor: '#0088af'},
      {id: 'SEGUROS AFIRME', url: 'assets/img/icons/afirme.svg', dynamicColor: '#009e10'},
      {id: 'ANA SEGUROS', url: 'assets/img/icons/ana.svg', dynamicColor: '#ff1135'},
      {id: 'AXA', url: 'assets/img/icons/axa.svg', dynamicColor: '#004c9c'},
      {id: 'BANORTE', url: 'assets/img/icons/banorte.svg', dynamicColor: '#e01f27'},
      {id: 'GENERAL DE SEGUROS', url: 'assets/img/icons/general.svg', dynamicColor: '#003389'},
      {id: 'GNP', url: 'assets/img/icons/gnp.svg', dynamicColor: '#ff8f02'},
      {id: 'HDI', url: 'assets/img/icons/hdi.svg', dynamicColor: '#009232'},
      {id: 'INBURSA', url: 'assets/img/icons/inbursa.svg', dynamicColor: '#002764'},
      {id: 'MAPFRE', url: 'assets/img/icons/mapfre.svg', dynamicColor: '#ff021b'},
      {id: 'MIGO', url: 'assets/img/icons/migo.svg', dynamicColor: '#ff6f0c'},
      {id: 'EL POTOSI', url: 'assets/img/icons/potosi.svg', dynamicColor: '#d31b21'},
      {id: 'QUALITAS', url: 'assets/img/icons/qualitas.svg', dynamicColor: '#762480'},
      {id: 'ZURA', url: 'assets/img/icons/sura.svg', dynamicColor: '#04b5cb'},
      {id: 'ZURICH', url: 'assets/img/icons/zurich.svg', dynamicColor: '#005bb2'},
      {id: 'EL AGUILA', url: 'assets/img/icons/zombi.svg', dynamicColor: '#023b81'},
      {id: 'AIG', url: 'assets/img/icons/aig.svg', dynamicColor: '#4ca3d9'},
      {id: 'LA LATINO', url: 'assets/img/icons/lalatino.svg', dynamicColor: '#426894'}
    ];
  }

  ngOnInit() {
    this.getPolizas();
  }
  getPolizas() {
    const usuario = JSON.parse(this.usuarioActivo);
    this.polizaService.getAllRegistroByIdUsuario(usuario.id).subscribe(data => {
      this.polizas = data;
      this.loaderCondition = true;
    });
  }
  getEndDate(fechaInicio): Date {
    const fechaFin = moment(fechaInicio);
    fechaFin.add(1, 'years');
    // @ts-ignore
    return fechaFin;
  }
  getImage(index: string) {
    return this.images.find((elementImg) => elementImg.id === index);
  }
  detallePoliza(idPoliza) {
    this.router.navigateByUrl('detalle-registro');
  }
  viewer(idArchivo) {
    this.router.navigateByUrl('ver-poliza-pdf/' + idArchivo);
  }
  getJsonObject(data: string) {
    return JSON.parse(data);
  }
  // BOTON DE AYUDA
  // openHelpModal() {
  //   Swal.fire( {
  //     title: 'ACCIONES',
  //     text: 'A continuación te indicamos para que sirven esos botones que tienes en tu póliza',
  //     html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
  //       '<br>' +
  //       '<div style="display: inline-flex;">' +
  //       '<span style="margin-right: 10px;" class="material-icons">description</span>' +
  //       '<div style="color: #014f98;">' +
  //       '<p>Con este botón podrá ver y descargar su póliza</p>' +
  //       '</div> ' +
  //       '</div>' +
  //       '<br>' +
  //       '</section>',
  //   });
  // }
}
