import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Poliza} from '../interfaces/poliza';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PolizaService {

    constructor(private http: HttpClient) {
    }

    getAllRegistroByIdUsuario(idCliente: number): Observable<Array<Poliza>> {
        return this.http.get<Array<Poliza>>(environment.apiMark + 'v1/app-cliente/get-registro-cliente/' + idCliente);
    }
    getAllRegistroById(idPoliza: number): Observable<Poliza> {
        return this.http.get<Poliza>(environment.apiMark + 'v1/app-cliente/get-registro/' + idPoliza);
    }
  async getDocument(data: string): Promise<any> {
    const resp = await this.http.get(`https://login-app.mark-43.net/auth-cliente/poliza/` + data, { responseType: 'blob' }).toPromise();
    return resp;
  }
}
