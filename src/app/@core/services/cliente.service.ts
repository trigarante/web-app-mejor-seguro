import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Cliente, ClienteDataLogin} from '../interfaces/cliente';
import {environment} from '../../../environments/environment';
import {Usuario} from '../../modulos/auth/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) {
  }
  getCLienteById(idCliente): Observable<Cliente> {
      return this.http.get<Cliente>(environment.apiMark + 'v1/cliente/' + idCliente);
  }
  putUpdatePassword(newPassword: string, uID: number): Observable<any> {
    const dataUpdate = { contrasenaApp: newPassword, acceso: 1 };
    return this.http.put<any>(`${environment.apiNode}/login/${uID}`, dataUpdate)
  }
}
