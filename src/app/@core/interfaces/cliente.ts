export interface Cliente {
    id: number;
    idPais: number;
    personaMoral: number;
    nombre: string;
    paterno: string;
    materno: string;
    cp: number;
    calle: string;
    numInt: string;
    numExt: string;
    idColonia: number;
    colonia: string;
    genero: string;
    telefonoFijo: string;
    telefonoMovil: string;
    correo: string;
    fechaNacimiento: string;
    fechaRegistro: string;
    curp: string;
    rfc: string;
    nombrePaises: string;
    razonSocial: string;
    archivo: string;
    archivoSubido: number;
    dni: string;
    ruc: string;
    idColoniaPeru: number;
}
export interface ClienteDataLogin {
    id: number;
    tipo: number;
    msg: string;
}
