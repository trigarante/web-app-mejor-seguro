export class ClienteClases {
   constructor() {
   }
  async validarExisteCliente(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const interval = setInterval(() => {
        const cliente = localStorage.getItem('cliente');
        if (cliente === null) {
        } else {
          clearInterval(interval);
          return resolve(true);
        }
      }, 50);
    });
  }
}
